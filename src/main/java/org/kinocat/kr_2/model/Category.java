package org.kinocat.kr_2.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class Category {
    @Getter
    final String singular;
    @Getter
    final String plural;

    public Category(String singular) {
        this.singular = singular;
        plural = singular + "s";
    }
}
