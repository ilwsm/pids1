package org.kinocat.kr_2.model;

import lombok.Getter;
import org.kinocat.kr_2.entities.Movie;
import org.kinocat.kr_2.entities.rent.Rent;
import org.kinocat.kr_2.entities.rent.User;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MovieRentInfo {
    private final Movie movie;
    @Getter
    private final int rentCount;

    @Getter
    private final Map<User, List<Rent>> map;

    public MovieRentInfo(Movie movie) {
        this.movie = movie;
        rentCount = calcRentCount();
        map = createUserRentMap();
    }

    public int avail() {
        return movie.getDiskCount() - rentCount;
    }

    private int calcRentCount() {
        return movie.getRents().stream().mapToInt(Rent::getDiskCount).sum();
    }

    private Map<User, List<Rent>> createUserRentMap() {
        //return movie.getRents().stream().collect(Collectors.toMap(Rent::getUser, Rent::getDiskCount, Integer::sum));
        return movie.getRents().stream()
                .collect(Collectors.groupingBy(Rent::getUser, LinkedHashMap::new, Collectors.toList()));
    }



}
