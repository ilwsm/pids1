package org.kinocat.kr_2.utils;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class Utils {

    public Boolean debt(LocalDateTime now, LocalDateTime end) {
        return now.isAfter(end);
    }
}