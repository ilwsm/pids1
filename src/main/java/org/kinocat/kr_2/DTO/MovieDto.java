package org.kinocat.kr_2.DTO;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MovieDto implements Serializable {
    private Long id;
    private String name;
    private Integer year;
    private Double rating;
    private String description;
    private String directorsStr;
    private String countriesStr;
    private String genresStr;
    private String posterPath;
    private int diskCount;
}