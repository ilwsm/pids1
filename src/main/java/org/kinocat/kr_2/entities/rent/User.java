package org.kinocat.kr_2.entities.rent;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "profit")
    private Double profit;

    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private Set<Rent> rents = new LinkedHashSet<>();

}