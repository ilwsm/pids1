package org.kinocat.kr_2.entities;

import java.util.Set;

public interface MyEntity {
    Long getId();

    void setId(Long id);
    Set<Movie> getMovies();

    void setName(String name);
}
