package org.kinocat.kr_2.services;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.json.JSONArray;
import org.json.JSONObject;
import org.kinocat.kr_2.DTO.MovieDto;
import org.kinocat.kr_2.entities.*;
import org.kinocat.kr_2.repositories.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class MovieService {
    final MovieRepository movieRepository;
    final DirectorRepository directorRepository;
    final GenreRepository genreRepository;
    final CountryRepository countryRepository;

    final JsonService jsonService;

    public List<Movie> findAll() {
        return movieRepository.findAll();
    }

    public List<Genre> findAllGenres() {
        return genreRepository.findAll();
    }

    public List<Director> findAllDirectors() {
        return directorRepository.findAll();
    }

    public List<Country> findAllCountries() {
        return countryRepository.findAll();
    }

    public Movie findById(long id) {
        return movieRepository.findById(id).orElse(null);
    }

    private Set<Director> directorsFrom(String str) {
        return getEntities(str, directorRepository, Director.class);
    }

    private Set<Genre> genresFrom(String str) {
        return getEntities(str, genreRepository, Genre.class);
    }

    private Set<Country> countriesFrom(String str) {
        return getEntities(str, countryRepository, Country.class);
    }

    @SneakyThrows
    private <T extends MyEntity> Set<T> getEntities(String str, JpaRepository<T, Long> repo, Class<T> clazz) {
        Set<T> mapped = new LinkedHashSet<>();
        if (!str.isEmpty()) {
            String[] strArr = str.split(",");
            Method findByNameIgnoreCase = repo.getClass().getMethod("findByNameIgnoreCase", String.class);
            for (String name : strArr) {
                name = name.trim();
                @SuppressWarnings("unchecked") T fromDb = (T) findByNameIgnoreCase.invoke(repo, name);
                if (fromDb != null) {
                    mapped.add(fromDb);
                } else {
                    T entity = clazz.getConstructor().newInstance();
                    entity.setName(name);
                    repo.save(entity);
                    mapped.add(entity);
                }
            }
        }
        return mapped;
    }

    public void add(MovieDto movieDto) {
        movieRepository.save(fromDtoWithDbUpdate(movieDto));
    }

    private static <T extends MyEntity> List<Long> collectOrphanIds(Set<T> oldEntities, Set<T> entities) {
        oldEntities.removeAll(entities);
        return collectOrphanIds(oldEntities);
    }

    private static <T extends MyEntity> List<Long> collectOrphanIds(Set<T> oldEntities) {
        return oldEntities.stream().filter(t -> t.getMovies().size() == 1).map(MyEntity::getId).toList();
    }

    public void update(MovieDto movieDto) {
        Movie movie = fromDtoWithDbUpdate(movieDto);

        // prepare orphan records
        Movie prevMovie = movieRepository.getReferenceById(movie.getId());
        List<Long> orphanDirectorIds = collectOrphanIds(prevMovie.getDirectors(), movie.getDirectors());
        List<Long> orphanGenreIds = collectOrphanIds(prevMovie.getGenres(), movie.getGenres());
        List<Long> orphanCountryIds = collectOrphanIds(prevMovie.getCountries(), movie.getCountries());

        movieRepository.save(movie);

        // remove orphan records
        directorRepository.deleteAllById(orphanDirectorIds);
        genreRepository.deleteAllById(orphanGenreIds);
        countryRepository.deleteAllById(orphanCountryIds);
    }

    public MovieDto toDto(Movie movie) {
        String directors = movie.getDirectors().stream().map(Director::getName).collect(Collectors.joining(", "));
        String countries = movie.getCountries().stream().map(Country::getName).collect(Collectors.joining(", "));
        String genres = movie.getGenres().stream().map(Genre::getName).collect(Collectors.joining(", "));

        return new MovieDto(
                movie.getId(), movie.getName(), movie.getYear(),
                movie.getRating(), movie.getDescription(),
                directors.trim(), countries.trim(), genres.trim(), movie.getPosterPath(), movie.getDiskCount());
    }

    public Movie fromDtoWithDbUpdate(MovieDto md) {
        Movie movie = new Movie();
        movie.setId(md.getId());
        movie.setName(md.getName());
        movie.setYear(md.getYear());
        movie.setRating(md.getRating());
        movie.setDescription(md.getDescription());
        movie.setPosterPath(md.getPosterPath());
        movie.setDiskCount(md.getDiskCount());
        movie.setDirectors(directorsFrom(md.getDirectorsStr()));
        movie.setGenres(genresFrom(md.getGenresStr()));
        movie.setCountries(countriesFrom(md.getCountriesStr()));
        return movie;
    }

    public void deleteById(long id) {
        // prepare orphan records
        Movie movie = movieRepository.getReferenceById(id);
        List<Long> orphanDirectorIds = collectOrphanIds(movie.getDirectors());
        List<Long> orphanGenreIds = collectOrphanIds(movie.getGenres());
        List<Long> orphanCountryIds = collectOrphanIds(movie.getCountries());

        movieRepository.deleteById(id);

        // remove orphan records
        directorRepository.deleteAllById(orphanDirectorIds);
        genreRepository.deleteAllById(orphanGenreIds);
        countryRepository.deleteAllById(orphanCountryIds);
    }

    public Collection<Movie> findByEntry(Map.Entry<String, String> entry) {
        return switch (entry.getKey()) {
            case "year" -> movieRepository.findByYear(Integer.parseInt(entry.getValue()));
            case "genre" -> genreRepository.findByNameIgnoreCase(entry.getValue()).getMovies();
            case "director" -> directorRepository.findByNameIgnoreCase(entry.getValue()).getMovies();
            case "country" -> countryRepository.findByNameIgnoreCase(entry.getValue()).getMovies();
            default -> new ArrayList<>();
        };
    }


    public Set<Map.Entry<Integer, Long>> groupYears() {
        Map<Integer, Long> map = movieRepository.findAll().stream()
                .filter(movie -> movie.getYear() != null)
                .collect(Collectors.groupingBy(Movie::getYear, Collectors.counting()));
        return map.entrySet();
    }

    @Transactional
    public void updateFromJson() {
        deleteAll();
        JSONArray jsonArray = jsonService.getJsonData();

        Map<String, Long> genreMap = new HashMap<>();
        Map<String, Long> directorMap = new HashMap<>();
        Map<String, Long> countryMap = new HashMap<>();
        Random rnd = new Random();

        for (Object object : jsonArray) {
            JSONObject jObject = (JSONObject) object;
            Movie movie = new Movie();
            movie.setDiskCount(rnd.nextInt(5) + 1);
            movie.setName(jObject.getString("title"));
            movie.setYear(jObject.getInt("year"));
            movie.setRating(jObject.getDouble("rating"));
            movie.setPosterPath(jObject.getString("poster_path"));
            movie.setDescription(jObject.getString("description"));
            updateMovieEntitiesFromJSON(Genre.class, genreMap, jObject.getJSONArray("genres"), genreRepository, movie.getGenres());
            updateMovieEntitiesFromJSON(Director.class, directorMap, jObject.getJSONArray("directors"), directorRepository, movie.getDirectors());
            updateMovieEntitiesFromJSON(Country.class, countryMap, jObject.getJSONArray("countries"), countryRepository, movie.getCountries());
            movieRepository.save(movie);
        }
    }

    @SneakyThrows
    private <T extends MyEntity> void updateMovieEntitiesFromJSON(Class<T> clazz, Map<String, Long> entityMap,
                                                                  JSONArray entityArray, JpaRepository<T, Long> repo, Set<T> movieEntities) {
        for (Object object : entityArray) {
            JSONObject jObject = (JSONObject) object;
            String name = jObject.getString("name");
            T entity = clazz.getConstructor().newInstance();
            entity.setName(name);
            Long id = entityMap.get(name);
            if (id != null) {
                entity.setId(id);
                movieEntities.add(entity);
            } else {
                T savedEntity = repo.save(entity);
                movieEntities.add(savedEntity);
                entityMap.put(name, savedEntity.getId());
            }
        }
    }

    public void deleteAll() {
        movieRepository.deleteAll();
        genreRepository.deleteAll();
        directorRepository.deleteAll();
        countryRepository.deleteAll();
    }
}
