package org.kinocat.kr_2.services;

import lombok.SneakyThrows;
import org.json.JSONArray;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.nio.file.Files;

@Component
public class JsonService {
    @SneakyThrows
    public JSONArray getJsonData() {
        File file = ResourceUtils.getFile("classpath:static/assets/movies.json");
        String content = Files.readString(file.toPath());
        return new JSONArray(content);
    }
}
