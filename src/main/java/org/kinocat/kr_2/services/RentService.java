package org.kinocat.kr_2.services;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.kinocat.kr_2.entities.Movie;
import org.kinocat.kr_2.entities.rent.Profit;
import org.kinocat.kr_2.entities.rent.Rent;
import org.kinocat.kr_2.entities.rent.User;
import org.kinocat.kr_2.model.MovieRentInfo;
import org.kinocat.kr_2.repositories.MovieRepository;
import org.kinocat.kr_2.repositories.ProfitRepository;
import org.kinocat.kr_2.repositories.RentRepository;
import org.kinocat.kr_2.repositories.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@AllArgsConstructor
@Service
public class RentService {
    ProfitRepository profitRepository;
    UserRepository userRepository;
    RentRepository rentRepository;
    MovieRepository movieRepository;

    @Transactional
    public void rent(String username, int count, int days, Movie movie, MovieRentInfo rentInfo) {
        if (rentInfo.avail() < count) throw new RuntimeException("The number of disks has been exceeded.");

        User user = userRepository.findByNameIgnoreCase(username);
        if (user == null) {
            user = new User();
            user.setName(username);
            user = userRepository.save(user);
        }
        Rent rent = new Rent();
        rent.setUser(user);
        LocalDateTime now = LocalDateTime.now();
        rent.setStart(now);
        rent.setEnd(now.plusMinutes(days)); // для учебных целей: минуты вместо дней
        rent.setMovie(movie);
        rent.setDiskCount(count);

        rentRepository.save(rent);
    }

    @Transactional
    @SneakyThrows
    public void rentOut(Long rentId) {
        Rent rent = rentRepository.findById(rentId).orElse(null);
        if (rent == null) throw new Exception("rent with id:" + rentId + " not found");
        LocalDateTime now = LocalDateTime.now();
        double profit;

        if (now.isAfter(rent.getEnd())) {
            profit = ChronoUnit.MINUTES.between(rent.getStart(), rent.getEnd()) * rent.getDiskCount(); //1 доллар за минуту )
            profit += ChronoUnit.MINUTES.between(rent.getEnd(), now) * 1.5 * rent.getDiskCount(); //1.5 доллара за просроченную минуту )
        } else {
            profit = ChronoUnit.MINUTES.between(rent.getStart(), now) * rent.getDiskCount();
        }

        User user = rent.getUser();
        double userProfit = user.getProfit() == null ? 0 : user.getProfit();
        user.setProfit(userProfit + profit);
        rentRepository.delete(rent);
        userRepository.save(user);
        Profit profitObject = getProfit();
        profitObject.setValue(profitObject.getValue() + profit);
        profitRepository.save(profitObject);
    }

    public Profit getProfit(){
        Profit profit = profitRepository.findFirstByIdNotNull();
        if (profit == null) {
            profit = new Profit();
            profit.setValue(0D);
            return profitRepository.save(profit);
        }
        return profit;
    }

    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    public void removeUserById(long userId) {
        userRepository.deleteById(userId);
    }

    public void resetProfit() {
        Profit profit = getProfit();
        profit.setValue(0D);
        profitRepository.save(profit);
    }
}
