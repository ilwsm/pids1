package org.kinocat.kr_2.controller;


import lombok.AllArgsConstructor;
import org.kinocat.kr_2.DTO.MovieDto;
import org.kinocat.kr_2.entities.Movie;
import org.kinocat.kr_2.model.Category;
import org.kinocat.kr_2.model.MovieRentInfo;
import org.kinocat.kr_2.services.MovieService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@Controller
@AllArgsConstructor
public class MovieController {
    final private MovieService movieService;
    @GetMapping("/movies_edit")
    public String movieEdit(Model model) {
        model.addAttribute("movies", movieService.findAll());
        return "/movies_edit";
    }

    @GetMapping("/movies")
    public String movies(Model model, @RequestParam Map<String, String> map) {
        if (map.isEmpty()) {
            model.addAttribute("movies", movieService.findAll());
        } else {
            Map.Entry<String, String> entry = map.entrySet().iterator().next();
            model.addAttribute("movies", movieService.findByEntry(entry));
            model.addAttribute("entry", entry);
        }

        return "movies";
    }

    @PostMapping("movies_edit")
    public String updateFromJson(Model model, @RequestParam String value) {
        if ("delete-all".equals(value)) {
            movieService.deleteAll();
        } else if ("update-from-json".equals(value)) {
            movieService.updateFromJson();
        }
        return "redirect:/movies_edit";
    }

    @GetMapping(value = "/years")
    public String years(Model model) {
        model.addAttribute("cat", new Category("year"));
        model.addAttribute("set", movieService.groupYears());
        return "movies_cats";
    }

    @GetMapping("/genres")
    public String genres(Model model) {
        model.addAttribute("cat", new Category("genre"));
        model.addAttribute("list", movieService.findAllGenres());
        return "movies_cats";
    }

    @GetMapping("/directors")
    public String directors(Model model) {
        model.addAttribute("cat", new Category("director"));
        model.addAttribute("list", movieService.findAllDirectors());
        return "movies_cats";
    }

    @GetMapping("/countries")
    public String countries(Model model) {
        model.addAttribute("cat", new Category("country", "countries"));
        model.addAttribute("list", movieService.findAllCountries());
        return "movies_cats";
    }

    @GetMapping("/movie")
    public String card(@RequestParam long id, Model model) {
        Movie movie = movieService.findById(id);
        if (movie == null) {
            return "redirect:/movies";
        }
        model.addAttribute("movie", movie);
        model.addAttribute("rentInfo", new MovieRentInfo(movie));
        return "card";
    }

    @RequestMapping(method = RequestMethod.POST, path = "/add_edit_movie")
    public String addEditMovie(@ModelAttribute MovieDto movieDto) {
        if (movieDto.getId() == null) {
            movieService.add(movieDto);
        } else {
            movieService.update(movieDto);
        }
        return "redirect:/movies_edit";
    }

    @GetMapping("/add")
    public String add(Model model) {
        MovieDto movieDto = new MovieDto();
        movieDto.setDiskCount(1);
        model.addAttribute("movieDto", movieDto);
        return "movie_addedit";
    }

    @GetMapping("/edit")
    public String edit(@RequestParam("id") long id, Model model) {
        Movie movie = movieService.findById(id);
        if (movie == null) return "redirect:/movies_edit";
        model.addAttribute("movieDto", movieService.toDto(movie));
        return "movie_addedit";
    }

    @GetMapping("delete")
    public String delete(@RequestParam("id") long id) {
        movieService.deleteById(id);
        return "redirect:/movies_edit";
    }
}
