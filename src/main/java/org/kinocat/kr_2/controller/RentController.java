package org.kinocat.kr_2.controller;

import lombok.AllArgsConstructor;
import org.kinocat.kr_2.entities.Movie;
import org.kinocat.kr_2.model.MovieRentInfo;
import org.kinocat.kr_2.services.MovieService;
import org.kinocat.kr_2.services.RentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import java.util.Map;

@Controller
@SessionAttributes({"movie", "rentInfo"})
@AllArgsConstructor
public class RentController {
    final private MovieService movieService;
    final private RentService rentService;


    @GetMapping("/users")
    public String movies(Model model) {
        model.addAttribute("users", rentService.findAllUsers());
        model.addAttribute("profit", rentService.getProfit().getValue());
        return "rent/users";
    }

    @PostMapping("/rent")
    public String rent(@RequestParam String username,
                       @RequestParam("disk-count") int count,
                       @RequestParam int days,
                       @ModelAttribute("movie") Movie movie, @ModelAttribute("rentInfo") MovieRentInfo rentInfo, SessionStatus sessionStatus) {


        try {
            rentService.rent(username, count, days, movie, rentInfo);
        } catch (RuntimeException e) {
            System.err.println(e.getMessage());
            return "redirect:/rent?movie_id=" + movie.getId();
        } finally {
            sessionStatus.setComplete();
        }

        return "redirect:/movie?id=" + movie.getId();
    }

    @PostMapping("/rent-out")
    public String rentOut(@RequestParam Map<String, String> rMap, SessionStatus status) {
        status.setComplete();
        Long rentId = Long.valueOf(rMap.get("rent_id"));
        rentService.rentOut(rentId);

        String page = rMap.get("page");
        return "redirect:" + page;
    }

    @PostMapping("/remove-user")
    public String removeUser(@RequestParam("user_id") long userId) {
        rentService.removeUserById(userId);
        return "redirect:/users";
    }

    @GetMapping("/rent")
    public String rent(Model model, @RequestParam("movie_id") Long movieId) {
        Movie movie = movieService.findById(movieId);
        if (movie == null) {
            return "redirect:/movies";
        }
        model.addAttribute("movie", movie);
        model.addAttribute("rentInfo", new MovieRentInfo(movie));
        return "rent/rent";
    }

    @GetMapping("/rent-out")
    public String rentOut(Model model, @RequestParam("movie_id") Long movieId) {
        Movie movie = movieService.findById(movieId);
        if (movie == null) {
            return "redirect:/movies";
        }
        model.addAttribute("movie", movie);
        model.addAttribute("rentInfo", new MovieRentInfo(movie));
        return "rent/return_rent";
    }

    @PostMapping("/reset-profit")
    @ResponseBody
    public void resetProfit(@RequestParam("reset") int value) {
        if (value == 1) {
            rentService.resetProfit();
        }
    }
}
