package org.kinocat.kr_2.repositories;

import org.kinocat.kr_2.entities.rent.Rent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RentRepository extends JpaRepository<Rent, Long> {
}