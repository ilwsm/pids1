package org.kinocat.kr_2.repositories;

import org.kinocat.kr_2.entities.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, Long> {
    List<Movie> findByYear(Integer year);
//    Movie findByYear(Integer year);
}