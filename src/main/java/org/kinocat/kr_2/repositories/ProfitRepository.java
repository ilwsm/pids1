package org.kinocat.kr_2.repositories;

import org.kinocat.kr_2.entities.rent.Profit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfitRepository extends JpaRepository<Profit, Double> {

    Profit findFirstByIdNotNull();

}