package org.kinocat.kr_2.repositories;

import org.kinocat.kr_2.entities.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {
    Country findByNameIgnoreCase(String name);
}