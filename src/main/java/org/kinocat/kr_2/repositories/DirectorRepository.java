package org.kinocat.kr_2.repositories;

import org.kinocat.kr_2.entities.Director;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DirectorRepository extends JpaRepository<Director, Long> {
    Director findByNameIgnoreCase(String name);
}