package org.kinocat.kr_2.repositories;

import org.kinocat.kr_2.entities.rent.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByNameIgnoreCase(String name);
}