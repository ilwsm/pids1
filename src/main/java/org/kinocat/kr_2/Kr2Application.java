package org.kinocat.kr_2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

@SpringBootApplication
public class Kr2Application {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(Kr2Application.class, args);
        openHomePage();
    }

    private static void openHomePage() throws IOException {
        if (!System.getProperty("os.name").startsWith("Windows")) return;
        Properties properties = new Properties();
        FileReader reader = new FileReader("src/main/resources/application.properties");
        properties.load(reader);
        reader.close();
        String port = properties.getProperty("server.port");
        if (port == null) port = "8080";

        Runtime rt = Runtime.getRuntime();
        rt.exec("cmd /c start msedge http://localhost:" + port + "");
    }
}
