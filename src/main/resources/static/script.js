function floatLabel(inputType) {
    let querySelector = document.querySelectorAll(inputType);
    querySelector.forEach((label) => {
        label.addEventListener("focus", (event) => {
            let target = event.target;
            target.nextElementSibling.classList.add("active");
        });
        if (label.value !== '' && label.value !== 'blank') {
            label.nextElementSibling.classList.add("active");
        }

        label.addEventListener("blur", (event) => {
            let target = event.target;
            if (target.value === '' || target.value === 'blank') {
                target.nextElementSibling.classList.remove("active");
            }
        });
    });
}

